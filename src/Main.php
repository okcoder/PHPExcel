<?php
/**
 * Created by PhpStorm.
 * @author OkCoder
 * @QQ 1046512080
 * @git https://gitee.com/okcoder
 * Date: 2018/12/25
 * Time: 15:00
 */

namespace OkCoder\PHPExcel;

class Main
{
    const HORIZONTAL = [
        'left' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'general' => \PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
        'right' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'center' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'centerContinuous' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS,
        'justify' => \PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
        'fill' => \PHPExcel_Style_Alignment::HORIZONTAL_FILL,
        'distributed' => \PHPExcel_Style_Alignment::HORIZONTAL_DISTRIBUTED
    ];

    private $writerType = 'Excel2007';   # Excel5 Excel2007

    public function __construct($writerType = 'Excel5')
    {
        $this->writerType = ucfirst($writerType);
    }

    /**
     * 导出
     * @param array $cells 标题行名称
     * @param array $list 导出数据
     * @param boolean $isDown 是否下载  false--保存   true--下载
     * @param string $fileName 文件名
     * @param string $savePath 保存路径
     * @param string $sheet_name sheet名称
     * @return string
     */
    public function export(array $cells = [], array $list = [], $isDown = false, $fileName = '', $savePath = './', $sheet_name = 'sheet')
    {
        // 实例化PHPExcel类
        $objPHPExcel = new \PHPExcel();
        // 激活当前的sheet表
        $objPHPExcel->setActiveSheetIndex(0);
        // 设置表格头（即excel表格的第一行）
        foreach ($cells as $key => $cell) {
            // 字母
            $letter = Int2Chr($key);
            $name = is_array($cell) ? $cell['name'] : $cell;
            $align = (is_array($cell) && isset($cell['align'])) ? $cell['align'] : 'left';
            $width = (is_array($cell) && isset($cell['width'])) ? $cell['width'] : 5;
            $objPHPExcel->setActiveSheetIndex(0)->getStyle($letter)
                ->getAlignment()
                ->setHorizontal(self::HORIZONTAL[$align]);
            $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($letter)->setWidth($width);

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . '1', $name);
        }
        // 循环刚取出来的数组，将数据逐一添加到excel表格。
        for ($i = 0; $i < count($list); $i++) {
            foreach ($cells as $key2 => $item2) {
                # 内容前面加上空格是为了防止数字被转为科学计算法显示
                $objPHPExcel->getActiveSheet()->setCellValue(Int2Chr($key2) . ($i + 2), ' ' . $list[$i][$key2]);
            }
        }
        // 设置当前激活的sheet表格名称；
        $objPHPExcel->getActiveSheet()->setTitle($sheet_name);

        //文件名处理
        $fileName = ($fileName ?: uniqid(time(), true)) . (strtolower($this->writerType) === 'excel5' ? '.xls' : '.xlsx');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $this->writerType);

        //清理缓冲区，避免中文乱码
        @ob_end_clean();

        if ($isDown) {
            //网页下载
            header('Cache-Control: max-age=0');
            header('pragma:public');
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Disposition:attachment;filename=$fileName");
            $objWriter->save('php://output');
            exit;
        }
        // 转码
        $_fileName = iconv("utf-8", "gb2312", $fileName);
        $_savePath = $savePath . $_fileName;
        $objWriter->save($_savePath);
        return $savePath . $fileName;

    }

    /**
     * 导入
     * @param string $file
     * @param array $fieldMapping 字段映射
     * @param int $sheet
     * @return array
     */
    public function import($file = '', array $fieldMapping = [], $sheet = 0)
    {
        //转码
        $file = iconv("utf-8", "gb2312", $file);
        if (empty($file) OR !file_exists($file)) {
            die('file not exists!');
        }
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        if (!$objReader->canRead($file)) {
            $objReader = \PHPExcel_IOFactory::createReader('Excel5');
            if (!$objReader->canRead($file)) {
                die('No Excel!');
            }
        }
        $objReader->setReadDataOnly(true);
        // 建立excel对象
        $objPHPExcel = $objReader->load($file);
        // 获取指定的sheet表
        $currSheet = $objPHPExcel->getSheet($sheet);
        // 取得最大的列号
        $columnH = $currSheet->getHighestColumn();
        // 获取总列数
        $columnCnt = Chr2Int($columnH) + 1;
        // 获取总行数
        $rowCnt = $currSheet->getHighestRow();
        $data = [];
        for ($_row = 1; $_row <= $rowCnt; $_row++) {  //读取内容
            for ($_column = 0; $_column < $columnCnt; $_column++) {
                $cellId = Int2Chr($_column) . $_row;
                $cellValue = $currSheet->getCell($cellId)->getValue();
                if ($cellValue instanceof PHPExcel_RichText) {   //富文本转换字符串
                    $cellValue = $cellValue->__toString();
                }
                $field = isset($fieldMapping[Int2Chr($_column)]) ? $fieldMapping[Int2Chr($_column)] : Int2Chr($_column);
                $data[$_row][$field] = $cellValue;
            }
        }
        return $data;
    }
}