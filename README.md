# PHPExcel

#### 介绍
PHPExcel 封装 一键导入导出

#### 安装教程

```
composer require okcoder/php-excel

```

#### 使用说明

1. 导入
```
$excel = new \OkCoder\PHPExcel\Main();
$ret = $excel->import('./15457201465c21d152642c53.90221892.xls', 0, [
    'A' => 'name',
    'B' => 'age',
    'C' => 'sex'
]);
dd($ret);

```
2. 导出
name:名称
width:宽度
align:left、center、right、general、centerContinuous、justify、fill、distributed
```
$excel = new \OkCoder\PHPExcel\Main('Excel2007'); // excel5 excel2007
$ret1 = $excel->export([['name' => '姓名', 'width' => 60], ['name' => '年龄', 'align' => 'center'], '性别'], [
    ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
], true);

$ret2 = $excel->export(['姓名', '年龄', '性别'], [
    ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
    , ['OkCoder', 27, '男']
], true);

dd($ret);   // 返回保存文件名称($isDown=true时无效)
```

### 赞助二维码

![微信打赏](https://images.gitee.com/uploads/images/2018/1228/122201_32d7aaa0_2159240.png "微信.png")
![支付宝打赏](https://images.gitee.com/uploads/images/2018/1228/122220_5d93117e_2159240.png "支付宝.png")